"use strict";
var Message = require('./Message')

class Agree extends Message {
  constructor(sender, receiver, conversationId, replyWith, inReplyTo) {
    super(sender, receiver, conversationId, replyWith, inReplyTo);
  }

  get performative() {
    return "agree";
  }
}

module.exports = Agree;
