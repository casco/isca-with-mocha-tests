"use strict";
var Message = require("./Message");
var Agree = require("./Agree");
var Propose = require("./Propose");

class CallForProposal extends Message {
  constructor(sender, receiver, conversationId, replyWith, inReplyTo) {
    super(sender, receiver, conversationId, replyWith, inReplyTo);
  }

  get performative() {
    return "cfp";
  }

  /**
   * prepares a template for an Agree massage from the receiver
   * THIS WILL NOT BE NECESARY IN THE FUTURE
   */
  asAgreeTemplate() {
    return new Agree(
      this.receiver,
      this.sender,
      this.conversationId,
      null,
      this.replyWith
    );
  }

  /**
   * prepares a template for an Propose message from the receiver
   */
  asProposeTemplate() {
    return new Propose(
      this.receiver,
      this.sender,
      this.conversationId,
      null,
      this.replyWith
    );
  }
}

module.exports = CallForProposal;
