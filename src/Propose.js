"use strict";
var Message = require('./Message')

class Propose extends Message {
  constructor(sender, receiver, conversationId, replyWith, inReplyTo) {
    super(sender, receiver, conversationId, replyWith, inReplyTo);
  }

  get performative() {
    return "propose";
  }
}

module.exports = Propose;
