"use strict";

class Conversation {
    
    /**
     * Create a new conversation
     * @param {String} conversationId - the id of the conversation
     */
    constructor(conversationId) {
        this.conversationId = conversationId;
        this.messages = []
    }

    addMessage(message) {
        this.messages.push(message);
    }

}

module.exports = Conversation;