"use strict";

class Courier {
  constructor() {
    this.outbox = [];
  }

  /**
   *
   * @param {message} Schedules the messave for delivery
   */
  send(message) {
    this.outbox.push(message);
  }

  /**
   * This one does the magic
   */
  deliverMessages() {
     // implement it in all subclases.
  }
  
}

module.exports = Courier;
