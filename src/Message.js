"use strict";

/**
 * A message as defined by FIPA : http://www.fipa.org/specs/fipa00061/SC00061G.html#_Toc26669710
 */
class Message {
  constructor(sender, receiver, conversationId, replyWith, inReplyTo) {
    //Denotes the identity of the sender of the message. A URL.
    this.sender = sender;
    //Denotes the identity of the receiver of the message. A URL.
    this.receiver = receiver;
    /**
     * Denotes the content of the message; equivalently denotes the object
     * of the action. The meaning of the content of any ACL message is intended
     * to be interpreted by the receiver of the message. A JS Object.
     */
    this.content = {};
    /**
     *  - the initiator of the protocol must assign a non-null value to the conversation-id parameter,
     *  - all responses to the message, within the scope of the same interaction protocol,
     *     should contain the same value for the conversation-id parameter
     *  - convention: the conversation-id is a concatenation of the initiator id, plus some unique string
     *      it generates
     */
    this.conversationId = conversationId;
    /**
     * The reply-with parameter is designed to be used to follow a conversation thread in a situation where multiple
     *  dialogues occur simultaneously. For example, if agent i sends to agent j a message which contains:
     *      reply-with <expr>
     *  Agent j will respond with a message containing:
     *      in-reply-to <expr>
     */
    this.replyWith = replyWith;
    this.inReplyTo = inReplyTo;
  }
  /**
   * Denotes the type of the communicative act of the ACL message
   * Subclasses should redefine this method
   */
  get performative() {
    return "undefined";
  }

  fromJSON(jsonString) {
    let obj = JSON.parse(jsonString);
    switch (obj.performative) {
      case "cfp":
        let instance = new CallForProposal();
        break;
      case "agree":
        let instance = new Agree(obj);
        break;
      default:
        return null;
    }

    Object.assign(instance, obj);
    return instance;
  }
}

class Agree extends Message {
  constructor(sender, receiver, conversationId, replyWith, inReplyTo) {
    super(sender, receiver, conversationId, replyWith, inReplyTo);
  }

  get performative() {
    return "agree";
  }
}

module.exports = Message
