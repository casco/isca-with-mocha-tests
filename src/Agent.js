"use strict";

var CallForProposal = require("../src/CallForProposal");
var Conversation = require("../src/Conversation");

class Agent {

  constructor(id, url) {
    this.id = id;
    this.url = url;
    this.title = "";
    this.description = "";
    this.product = "";
    this.stock = 0;
    this.lastConversationNumber = 0;
    this.lastReplyToNumber = 0;
    this.conversations = {};
  }

  newCfp(receiverUrl) {
    this.lastConversationNumber = this.lastConversationNumber + 1;
    this.lastReplyToNumber = this.lastReplyToNumber + 1;
    let cid = this.url + "/conversations/" + this.lastConversationNumber;
    return new CallForProposal(
      this.url,
      receiverUrl,
      cid,
      "" + this.lastReplyToNumber,
      null
    );
  }

  /**
   * send and record a message
   * @param {Message} message - the message to send and record in a conversation
   * @param {Courier} courier - the courier to use to send the message 
   */
  send(message, courier) {
    courier.send(message);
    this.record(message);
  }

  /**
   * record a message as part of a conversation
   * @param {Message} message - the message to record
   */
  record(message) {
    if (! this.conversations[message.conversationId]) {
      this.conversations[message.conversationId] = new Conversation(message.conversationId)
    };
    this.conversations[message.conversationId].addMessage(message);
  }

  /**
   * receive a cfp - if stock is available answer with a propose.
   * if stock is not enough, send cfp to all my providers, for the 
   * required stock 
   * @param {CallForProposal} cfp - the call for proposals received 
   * @param {Courrier} courrier - the courrier to use to send messages
   */
  receiveCfp(cfp, courrier) {
    this.record(cfp);
    if (cfp.content.quantity <= this.stock) {
      let propose = cfp.asProposeTemplate();
      courrier.send(propose);
    } 

  }
}

module.exports = Agent;
