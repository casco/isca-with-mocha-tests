var assert = require("assert");
var CallForProposal = require("../src/CallForProposal");

var someSenderId = "http://localhost/agents/1";
var someConversationId = "http://localhost/agents/1/conversations/1";
var someReceiverId = "http://localhost/agents/2";
var someReplyWith = "1";
var cfp = new CallForProposal(
  someSenderId,
  someReceiverId,
  someConversationId,
  someReplyWith,
  null
);

describe("CallForProposal", function() {
  describe("new CallForProposal(...)", function() {
    it(
      "should return an instance of CallForProposal, with the given sender, receiver conversation-id," +
        " and replyTo, with empty content, and null replyWith and inReplyTo",
        function() {
        assert.equal(cfp.performative, "cfp");
        assert.equal(cfp.sender, someSenderId);
        assert.equal(cfp.receiver, someReceiverId);
        assert.equal(cfp.conversationId, someConversationId);
        assert.deepEqual(cfp.content, {});
        assert.equal(cfp.replyWith, someReplyWith);
        assert.equal(cfp.inReplyTo, null);
      }
    );
  });

  describe("asAgreeTemplate()", function() {
    it(
      "should return an Agree message, prepared to respond to this cfp" +
        " with sender and receiver inverted, same conversation-id, and a matching inReplyTo",
        function() {
        let agree = cfp.asAgreeTemplate();
        assert.equal(agree.performative, "agree");
        assert.equal(agree.sender, someReceiverId);
        assert.equal(agree.receiver, someSenderId);
        assert.equal(agree.conversationId, someConversationId);
        assert.deepEqual(agree.content, {});
        assert.equal(agree.replyWith, null);
        assert.equal(agree.inReplyTo, someReplyWith);
      }
    );
  });

  describe("asProposeTemplate()", function() {
    it(
      "should return an Propose message, prepared to respond to this cfp" +
        " with sender and receiver inverted, same conversation-id, and a matching inReplyTo",
        function() {
        let agree = cfp.asProposeTemplate();
        assert.equal(agree.performative, "propose");
        assert.equal(agree.sender, someReceiverId);
        assert.equal(agree.receiver, someSenderId);
        assert.equal(agree.conversationId, someConversationId);
        assert.deepEqual(agree.content, {});
        assert.equal(agree.replyWith, null);
        assert.equal(agree.inReplyTo, someReplyWith);
      }
    );
  });
});
