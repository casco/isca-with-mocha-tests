var assert = require("assert");
var Agent = require("../src/Agent");
var DummyCourier = require("./DummyCourier");

var someSenderUrl = null;
var someReceiverUrl = null;
var aSendingAgent = null;
var aReceivingAgent = null;
var courier = null;

describe("Agent", function() {
  beforeEach(function() {
    someSenderUrl = "http://localhost/agents/1";
    someReceiverUrl = "http://localhost/agents/2";
    aSendingAgent = new Agent("1", someSenderUrl);
    aReceivingAgent = new Agent("2", someReceiverUrl);
    courier = new DummyCourier();
  });

  describe("new Agent(...)", function() {
    it("should return a instante initialized with the given id and url ", function() {
      assert.equal(aSendingAgent.id, "1");
      assert.equal(aSendingAgent.url, someSenderUrl);
      assert.deepEqual(aSendingAgent.conversations, []);
      assert.equal(aSendingAgent.stock, 0);
      assert.equal(aSendingAgent.product, "");
    });
  });

  describe("newCfp()", function() {
    it(
      "should return a cfp, with the agent as the sender, with the indicated receiver" +
        " with a new conversation id, and a new replyWith code",
      function() {
        let cfp = aSendingAgent.newCfp(someReceiverUrl);
        assert.equal(cfp.sender, aSendingAgent.url);
        assert.equal(cfp.receiver, someReceiverUrl);
        assert.equal(
          cfp.conversationId,
          "http://localhost/agents/1/conversations/1"
        );
        assert.equal(cfp.replyWith, "1");
      }
    );
  });

  describe("send(message, courier)", function() {
    it(
      "the courier should have a new message in its outbox and " +
        "the message should be recorded in a conversation at the agent",
      function() {
        let cfp = aSendingAgent.newCfp(someReceiverUrl);
        aSendingAgent.send(cfp, courier);
        assert.equal(courier.outbox.length, 1);
        assert.equal(
          aSendingAgent.conversations[cfp.conversationId].conversationId,
          cfp.conversationId
        );
      }
    );
  });

  describe("receiveCfp(cfp, courier), when stock is available", function() {
    it(
      "should ask the courier to send an agree to the sender of the cfp" +
        " - the inReplyTo of the propose, should match the replyWith of the cfp",
      function() {
        let cfp = aSendingAgent.newCfp(someReceiverUrl);
        cfp.content = { quantity: 1 };
        aReceivingAgent.stock = 1;
        aReceivingAgent.receiveCfp(cfp, courier);
        assert.equal(courier.outbox[0].performative, "propose");
        assert.equal(courier.outbox[0].inReplyTo, cfp.replyWith);
      }
    );
  });

  describe("receiveCfp(cfp, courier), without stock and two providers", function() {
    it("should ask the courier to send cfp to all providers of the agent");
  });
});
