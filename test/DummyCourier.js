"use strict";

var Courier = require("../src/Courier");

class DummyCourier extends Courier {
  constructor() {
    super();
  }

  deliverMessages() {
    // implement it in all subclases.
  }
}

module.exports = DummyCourier;
